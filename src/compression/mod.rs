use std::io::prelude::*;

use flate2::Compression;
use flate2::read::DeflateDecoder;
use flate2::write::DeflateEncoder;

use base64::{encode, decode};

use tracing::{event, Level, span};

#[cfg(test)]
use mockall::automock;

#[cfg_attr(test, automock)]
pub(crate) trait CompressionService {
    fn to_base64(&self, array: &[u8]) -> String;
    fn from_base64(&self, base64: &str) -> Option<Vec<u8>>;

    fn deflate(&self, json : &str) -> Option<Vec<u8>>;
    fn inflate(&self, array : &[u8]) -> Option<String>;
}

pub(crate) struct CompressionServiceImpl;
impl CompressionService for CompressionServiceImpl {
    fn to_base64(&self, array: &[u8]) -> String {
        let _ = span!(Level::DEBUG, "to_base64").entered();

        encode(array)
    }

    fn from_base64(&self, base64: &str) -> Option<Vec<u8>> {
        let _ = span!(Level::DEBUG, "from_base64").entered();

        match decode(base64) {
            Ok(vec) => Some(vec),
            Err(err) => {
                event!(Level::ERROR, "Cannot decode the requested base64 string: {}", base64);
                event!(Level::ERROR, "  >> {}", err);
                None
            }
        }
    }

    fn deflate(&self, json : &str) -> Option<Vec<u8>> {
        let _ = span!(Level::DEBUG, "deflate").entered();

        let mut encoder = DeflateEncoder::new(Vec::new(), Compression::best());
        let result = encoder.write_all(json.as_bytes());

        match result {
            Ok(..) => match encoder.finish() {
                Ok(vec) => Some(vec),
                Err(err) => {
                    event!(Level::ERROR, "Cannot deflate the requested byte array");
                    event!(Level::ERROR, "  >> {}", err);
                    None
                }
            },
            Err(err) => {
                event!(Level::ERROR, "An error occurred while trying to deflate the requested byte array");
                event!(Level::ERROR, "  >> {}", err);
                None
            }
        }
    }

    fn inflate(&self, array : &[u8]) -> Option<String> {
        let _ = span!(Level::DEBUG, "inflate").entered();

        let mut decoder = DeflateDecoder::new(array);
        let mut string = String::new();
        match decoder.read_to_string(&mut string) {
            Ok(..) => Some(string),
            Err(err) => {
                event!(Level::ERROR, "Cannot inflate the requested byte array");
                event!(Level::ERROR, "  >> {}", err);
                None
            }
        }
    }
}


#[cfg(test)]
mod test {
    use std::borrow::Borrow;
    use super::*;
    
    const super_impl: CompressionServiceImpl = CompressionServiceImpl {};

    #[test_log::test]
    fn base64() {
        let good_string = ("I'm a good String", "SSdtIGEgZ29vZCBTdHJpbmc=");
        let result = super_impl.from_base64(good_string.1);
        assert!(result.is_some(), "A result should be present");
        let string = std::str::from_utf8(result.as_ref().unwrap().borrow());
        assert!(string.is_ok(), "The result should be a valid String");
        assert_eq!(good_string.0, string.unwrap(), "The String should be the correct one");

        let bad_string = "8LJ?t@3B#fA0=fmEbTE(";
        assert!(super_impl.from_base64(bad_string).is_none(), "The ASCII85 encoding should be detected as an invalid BASE64");
    }

    #[test_log::test]
    fn deflate() {
        let good_string : (&str, [u8; 27]) = ("I'm a compressible String", [ 0xf3, 0x54, 0xcf, 0x55,
            0x48, 0x54, 0x48, 0xce, 0xcf, 0x2d, 0x28, 0x4a, 0x2d, 0x2e, 0xce, 0x4c, 0xca, 0x49, 0x55,
            0x08, 0x2e, 0x29, 0xca, 0xcc, 0x4b, 0x07, 0x00]);
        let result = super_impl.deflate(good_string.0);
        assert!(result.is_some(), "A result should be present");
        let vec = result.as_ref().unwrap();
        assert_eq!(vec.len(), good_string.1.len(), "The result size should be the same");
        for i in 0..vec.len() {
            assert_eq!(vec[i], good_string.1[i], "All byte of the result should be equals to the correct one");
        }
    }

    #[test_log::test]
    fn inflate() {
        let good_array : (&str, [u8; 25]) = ("I'm a compressed String", [ 0xf3, 0x54, 0xcf, 0x55,
            0x48, 0x54, 0x48, 0xce, 0xcf, 0x2d, 0x28, 0x4a, 0x2d, 0x2e, 0x4e, 0x4d, 0x51, 0x08, 0x2e,
            0x29, 0xca, 0xcc, 0x4b, 0x07, 0x00 ]);
        let result = super_impl.inflate(good_array.1.borrow());
        assert!(result.is_some(), "A result should be present");
        assert_eq!(good_array.0, result.unwrap(), "The result should be exactly the same as the correct String");

        let bad_array : [u8; 1] = [ 0xff ];
        assert!(super_impl.inflate(bad_array.borrow()).is_none(), "An invalid array shouldn't be able to pass decompression");
    }
}