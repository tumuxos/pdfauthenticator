use tracing::{event, Level, span};

#[cfg(test)]
use mockall::automock;


#[cfg_attr(test, automock)]
pub(crate) trait CryptoService {
    fn encrypt(&self, array: &[u8]) -> Option<(Vec<u8>, Vec<u8>)>;
    fn decrypt(&self, array: &[u8], iv: &[u8]) -> Result<Vec<u8>, ()>;

    fn sign(&self, array: &[u8]) -> Option<(Vec<u8>, String)>;
    fn verify(&self, array: &[u8], signature: &[u8], key: &str) -> Result<(), ()>;
}

pub(crate) struct CryptoServiceImpl;
impl CryptoService for CryptoServiceImpl {
    fn encrypt(&self, array: &[u8]) -> Option<(Vec<u8>, Vec<u8>)>  {
        let _ = span!(Level::DEBUG, "encrypt").entered();
        None
    }

    fn decrypt(&self, array: &[u8], iv: &[u8]) -> Result<Vec<u8>, ()> {
        let _ = span!(Level::DEBUG, "decrypt").entered();
        Err(())
    }

    fn sign(&self, array: &[u8]) -> Option<(Vec<u8>, String)> {
        let _ = span!(Level::DEBUG, "sign").entered();
        None
    }

    fn verify(&self, array: &[u8], signature: &[u8], key: &str) -> Result<(), ()> {
        let _ = span!(Level::DEBUG, "verify").entered();
        Err(())
    }
}