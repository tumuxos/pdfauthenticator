use std::{
    convert::Infallible,
    future::{ready, Ready},
};

use actix_web::{dev, http::header::AcceptLanguage, FromRequest, HttpMessage as _, HttpRequest};
use fluent_templates::LanguageIdentifier;
use serde::Serialize;

#[derive(Debug, Serialize)]
#[serde(transparent)]
pub(crate) struct LangChoice(String);

pub(crate) trait LangChoiceTrait {
    fn from_req(req: &HttpRequest) -> Self;
    fn lang_id(&self) -> LanguageIdentifier;
}

impl LangChoiceTrait for LangChoice {
    fn from_req(req: &HttpRequest) -> Self {
        // Try to read form a possible lang=xx parameter
        let uri = req.uri().to_string();
        let split_prefix : Vec<&str> = uri.split("lang=").collect();
        let split_suffix : Option<Vec<&str>> = if split_prefix.len() > 1 {
            Some(split_prefix[1].split("&").collect())
        } else {
            None
        };

        // ?lang= will always have precedent over AcceptLanguage header tag
        let lang = if split_suffix.is_some()
            && !split_suffix.as_ref().unwrap().is_empty()
            && !split_suffix.as_ref().unwrap()[0].is_empty() {
            split_suffix.unwrap()[0].to_string()
        } else {
            req.get_header::<AcceptLanguage>()
                .and_then(|lang| lang.preference().into_item())
                .map_or_else(|| "en".to_owned(), |lang| lang.to_string())
        };

        Self(lang)
    }

    fn lang_id(&self) -> LanguageIdentifier {
        // unwrap: lang ID should be valid given extraction method
        self.0.parse().unwrap()
    }
}

impl FromRequest for LangChoice {
    type Error = Infallible;
    type Future = Ready<Result<Self, Self::Error>>;

    fn from_request(req: &HttpRequest, _pl: &mut dev::Payload) -> Self::Future {
        ready(Ok(Self::from_req(req)))
    }
}
