use std::borrow::Borrow;
use std::io::{Error, ErrorKind};
use std::path::PathBuf;
use std::sync::{Arc, Mutex};

use actix_web::{App, HttpServer, HttpResponse};
use actix_web::body::BoxBody;
use actix_web::dev::ServiceResponse;
use actix_web::http::StatusCode;
use actix_web::http::header::ContentType;
use actix_web::middleware::{ErrorHandlerResponse, ErrorHandlers};
use clap::{arg, Arg, ArgAction, ArgMatches, Parser, value_parser};
use fluent_templates::{static_loader, Loader as _};
use lazy_static::lazy_static;
use tracing::{Level, instrument, event, span};
use tracing_actix_web::TracingLogger;
use tracing_subscriber::FmtSubscriber;

mod compression;
mod controller;
mod crypto;
mod utils;

use utils::lang_choice::{LangChoice, LangChoiceTrait};
use crate::utils::lang_choice;

static_loader! {
    static LOCALES = {
        locales: "./resources/locales",
        fallback_language: "en"
    };
}

#[derive(Clone)]
pub(crate) struct Context<'a> {
    compression_service: &'a (dyn compression::CompressionService + Sync),
    crypto_service: &'a (dyn crypto::CryptoService + Sync),
}

lazy_static! {
    static ref CONFIG: (ArgMatches, Context<'static>) = init();
}

#[tracing::instrument]
fn init<'a>() -> (ArgMatches, Context<'a>) {
    let args = argfile::expand_args_from(
        wild::args_os(),
        argfile::parse_fromfile,
        argfile::PREFIX,
    ).unwrap();

    let clap = clap::App::new(env!("CARGO_PKG_NAME")).version(env!("CARGO_PKG_VERSION")).author(env!("CARGO_PKG_AUTHORS"))
        .about("A program to generate a QRCODE for document that can help authenticate those documents and help prove their non-tampered state")
        .arg(Arg::new("ip").short('i').long("ip")
            .default_value("127.0.0.1").help("IP on witch to listen to")
            .takes_value(true).value_name("IP"))
        .arg(Arg::new("port").short('p').long("port")
            .default_value("8080").help("PORT on witch to listen to")
            .value_parser(value_parser!(u16).range(1..))
            .takes_value(true).value_name("PORT"))
        .arg(Arg::new("endpoint").short('e').long("endpoint")
            .required(true).help("Endpoint where the validation will be accessible")
            .takes_value(true).value_name("ENDPOINT"))

        .arg(Arg::new("private key").long("priv-key")
            .required(true).help("The private key to use for the encryption/signature")
            .value_parser(value_parser!(PathBuf))
            .takes_value(true).value_name("PRIVATE KEY FILE"))
        .arg(Arg::new("public key").long("pub-key")
            .required(false).help("The public key to use in case of signature")
            .value_parser(value_parser!(PathBuf))
            .takes_value(true).value_name("PUBLIC KEY FILE"))
        .arg(Arg::new("signed").short('s').long("signed")
            .action(ArgAction::SetTrue).help("Use the signature system instead of the embedded encryption"))

        .arg(Arg::new("logo path").short('l').long("logo")
            .value_parser(value_parser!(PathBuf))
            .required(false).help("Path to the custom logo to append on top of the QRCODE")
            .takes_value(true).value_name("LOGO FILE PATH"))
        .arg(Arg::new("logo size").long("logo-size")
            .value_parser(value_parser!(u32).range(32..))
            .required(false).help("Size of the selected logo (must be a squared image)")
            .takes_value(true).value_name("LOGO SIZE (px)"))

        .arg(Arg::new("no frame").short('n').long("no-frame")
            .action(ArgAction::SetTrue).help("Disable the frame drawing around the QRCODE"))
        .arg(Arg::new("frame color").short('c').long("frame-color").default_value("#4177C9")
            .required(false).help("Custom color to use for the frame around the QRCODE")
            .takes_value(true).value_name("#RRGGBB OR #RGB COLOR CODE"))
        .arg(Arg::new("frame label").long("frame-label")
            .required(false).help("Custom label to use for the frame")
            .takes_value(true).value_name("LABEL"))
        .arg(Arg::new("label on top").short('t').long("label-top")
            .action(ArgAction::SetTrue).help("Put the label on top of the QRCODE instead of the bottom"))

        .arg(Arg::new("verbosity").short('v').long("verbose")
            .action(ArgAction::Count).help("Set the verbosity level"))
        .get_matches_from(args);

    (clap, Context {
        compression_service: &compression::CompressionServiceImpl {},
        crypto_service: &crypto::CryptoServiceImpl {}
    })
}

fn check_rgb_color_code(s: &str) -> Result<&str, String> {
    let err = "The RGB color code should be #RRGGBB or #RGB".to_string();

    if s.len() != 4 && s.len() != 7 {
        return Err(err);
    } else if !s.is_ascii() {
        return Err(err);
    } else if s.chars().nth(0).expect(&*err) != '#' {
        return Err(err);
    } else {
        let split: Vec<&str> = s.split('#').collect();
        let _rgb: u32 = split[1].parse().map_err(|_| err)?;
    }

    Ok(s)
}

#[tracing::instrument]
#[actix_web::main]
async fn main() -> std::io::Result<()> {
    // Load all arguments from the env, the command line and/or the configuration file
    let verbosity = match CONFIG.0.get_one::<u8>("verbosity").expect("Count's are defaulted") {
        0 => Level::INFO,
        1 => Level::DEBUG,
        _ => Level::TRACE
    };

    // Initialize the log system
    let subscriber = FmtSubscriber::builder()
        .with_max_level(verbosity)
        .finish();
    tracing::subscriber::set_global_default(subscriber)
        .expect("setting default subscriber failed");
    event!(Level::INFO, "Starting {} version {}", env!("CARGO_PKG_NAME"), env!("CARGO_PKG_VERSION"));

    // Initialize the web framework
    let ip = CONFIG.0.get_one::<String>("ip").expect("defaulted by clap");
    let port = CONFIG.0.get_one::<u16>("port").expect("defaulted by clap");
    event!(Level::DEBUG, "Actix-web will listen to {}:{}", ip, port);
    HttpServer::new(|| {
       App::new()
           .wrap(error_handlers())
           .wrap(TracingLogger::default())
           .service(controller::status)
           .service(controller::create)
           .app_data(CONFIG.clone())
    })
        .bind((ip.clone(), port.clone()))?
        .run()
        .await
}

fn error_handlers() -> ErrorHandlers<BoxBody> {
    ErrorHandlers::new()
        .handler(StatusCode::BAD_REQUEST, bad_request)
        .handler(StatusCode::FORBIDDEN, forbidden)
        .handler(StatusCode::NOT_FOUND, not_found)
        .handler(StatusCode::METHOD_NOT_ALLOWED, not_allowed)
        .handler(StatusCode::NOT_ACCEPTABLE, not_acceptable)
        .handler(StatusCode::INTERNAL_SERVER_ERROR, internal_server_error)
}

fn error_handler<B>(service_response: ServiceResponse<B>, key: &str) -> Result<ErrorHandlerResponse<BoxBody>, actix_web::Error> {
    let lang = LangChoice::from_req(service_response.request()).lang_id();
    let error = LOCALES.lookup(&lang, key);

    let response = HttpResponse::build(service_response.status())
        .content_type(ContentType::plaintext())
        .body(error.to_string());

    Ok(ErrorHandlerResponse::Response(ServiceResponse::new(
        service_response.into_parts().0,
        response.map_into_left_body(),
    )))
}

// 400
fn bad_request<B>(res: ServiceResponse<B>) -> Result<ErrorHandlerResponse<BoxBody>, actix_web::Error> {
    error_handler(res, "error-bad-request")
}

// 403
fn forbidden<B>(res: ServiceResponse<B>) -> Result<ErrorHandlerResponse<BoxBody>, actix_web::Error> {
    error_handler(res, "error-forbidden")
}

// 404
fn not_found<B>(res: ServiceResponse<B>) -> Result<ErrorHandlerResponse<BoxBody>, actix_web::Error> {
    error_handler(res, "error-not-found")
}

// 405
fn not_allowed<B>(res: ServiceResponse<B>) -> Result<ErrorHandlerResponse<BoxBody>, actix_web::Error> {
    error_handler(res, "error-not-allowed")
}

// 406
fn not_acceptable<B>(res: ServiceResponse<B>) -> Result<ErrorHandlerResponse<BoxBody>, actix_web::Error> {
    error_handler(res, "error-not-acceptable")
}

// 500
fn internal_server_error<B>(res: ServiceResponse<B>) -> Result<ErrorHandlerResponse<BoxBody>, actix_web::Error> {
    error_handler(res, "error-internal-server-error")
}