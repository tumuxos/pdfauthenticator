use std::borrow::Borrow;
use std::collections::HashMap;
use std::ops::Add;

use actix_web::{get, post, HttpResponse, Responder, HttpRequest, web};

use bstr::ByteSlice;

use chrono::Local;

use clap::ArgMatches;
use clap::builder::TypedValueParser;

use fluent_templates::fluent_bundle::FluentValue;
use fluent_templates::Loader;

use futures_util::StreamExt;
use json::object;

use tracing::{event, Level};

use urlencoding::{decode, encode};

use crate::{Context, LOCALES};
use crate::compression::*;
use crate::crypto::*;
use crate::utils::lang_choice::*;

#[get("/status")]
pub async fn status(req: HttpRequest) -> impl Responder {
    let args: HashMap<&str, FluentValue> = HashMap::from([
        ("name", env!("CARGO_PKG_NAME").into()),
        ("version", env!("CARGO_PKG_VERSION").into()),
    ]);

    let lang = LangChoice::from_req(&req).lang_id();
    LOCALES.lookup_with_args(&lang, "status", &args)
}

#[post("/create")]
pub async fn create(req: HttpRequest, mut payload: web::Payload) -> impl Responder {
    let conf = req.app_data::<(ArgMatches, Context)>().expect("Should have a configuration struct");
    let lang = LangChoice::from_req(&req).lang_id();

    // payload is a stream of Bytes objects
    let mut body = web::BytesMut::new();
    while let Some(chunk) = payload.next().await {
        if chunk.is_err() {
            event!(Level::ERROR, "Cannot read bytes from socket");
            //FIXME: lib seem to Derive Display but missing something for it to work
            // event!(Level::ERROR, "  >> {}", chunk.unwrap_err());
            return HttpResponse::InternalServerError().finish();
        }
        body.extend_from_slice(chunk.as_ref().unwrap());
    }

    // Try to convert the byte we just received into a string
    let array = body.freeze();
    let str = array.to_str();
    if str.is_err() {
        event!(Level::ERROR, "Cannot parse the bytes array into a String");
        event!(Level::ERROR, "  >> {}", str.unwrap_err());
        return HttpResponse::BadRequest().body(LOCALES.lookup(&lang, "cannot-parse-bytes"));
    }

    // Convert the String into a JSON Object
    let json = json::parse(str.unwrap());
    if json.is_err() {
        event!(Level::ERROR, "Cannot parse the String into a JSON object");
        event!(Level::ERROR, "  >> {}", json.unwrap_err());
        return HttpResponse::BadRequest().body(LOCALES.lookup(&lang, "cannot-parse-json"));
    }
    let mut json = json.unwrap();

    let mut keys = "".to_string();
    for element in json.entries() {
        let k = keys.to_string() + " ";
        keys = k + element.0;
    }
    event!(Level::INFO, "JSON is {{{} }}", keys);

    // Append some information into the JSON before sealing it
    let timestamp = Local::now().to_string();
    let err = json.insert("__TIMESTAMP", timestamp.as_str());
    if err.is_err() {
        event!(Level::ERROR, "Cannot add the TIMESTAMP");
        event!(Level::ERROR, "  >> {}", err.unwrap_err());
        return HttpResponse::InternalServerError().finish();
    }
    event!(Level::INFO, "Sealed timestamp is {}", timestamp.to_string());

    // First we need to compress that JSON (or text?) to gain space
    let compressed = conf.1.compression_service.deflate(json.dump().as_str());
    if compressed.is_none() {
        return HttpResponse::InternalServerError().finish();
    }
    let compressed = compressed.unwrap();

    // Test if we need to use the encryption method or the signature one
    let signed = conf.0.get_one::<bool>("signed").copied();
    let qrcode_data = if signed.is_none() || !signed.unwrap() {
        // Then we encrypt the processed data
        let encrypted = conf.1.crypto_service.encrypt(&*compressed);
        if encrypted.is_none() {
            return HttpResponse::InternalServerError().finish();
        }

        // After that, we convert that encrypted blob into a base64 string
        let encrypted_base64 = conf.1.compression_service.to_base64(&encrypted.as_ref().unwrap().0);
        let random_base64 = conf.1.compression_service.to_base64(&encrypted.as_ref().unwrap().1);

        // Finally we need to create an URL with that payload and the program's version for the QRCODE
        String::from(conf.0.get_one::<String>("endpoint").expect("The endpoint is mandatory"))
            .add("?v=").add(encode(env!("CARGO_PKG_VERSION")).borrow())
            .add("&d=").add(&*encrypted_base64)
            .add("&r=").add(&*random_base64)
    } else {
        // Then we need to hash and sign that processed data
        let signature = conf.1.crypto_service.sign(&*compressed);
        if signature.is_none() {
            return HttpResponse::InternalServerError().finish();
        }

        // After that we need to create a new JSON that will contain the payload, the signature, the key alias and the version
        let payload_base64 = conf.1.compression_service.to_base64(&*compressed);
        let signature_base64 = conf.1.compression_service.to_base64(&signature.as_ref().unwrap().0);
        let new_json = object!{
            "version" => env!("CARGO_PKG_VERSION"),
            "payload" => payload_base64,
            "signature" => signature_base64,
            "key_alias" => signature.unwrap().1.clone(),
        };
        let new_compressed = conf.1.compression_service.deflate(new_json.dump().as_str());
        if new_compressed.is_none() {
            event!(Level::ERROR, "Cannot process the new JSON while performing the signature creation");
            return HttpResponse::InternalServerError().finish();
        }

        // Finally we convert that compressed JSON into a base64 string
        conf.1.compression_service.to_base64(&*new_compressed.unwrap())
    };

    // Generate the QRCODE based on the data we just got
    //TODO


    event!(Level::WARN, "NOT IMPLEMENTED");
    HttpResponse::NotImplemented().body(qrcode_data)
}

#[cfg(test)]
mod test {
    use std::sync::Mutex;
    use actix_web::{App, HttpRequest, test};
    use actix_web::http::{header, StatusCode};
    use clap::{Arg, ArgAction, ArgMatches};
    use const_format::concatcp;
    use lazy_static::lazy_static;
    use fluent_templates::LanguageIdentifier;
    use mockall::predicate::*;
    use mockall::predicate::eq;

    use crate::controller::MockCompressionService;
    use crate::crypto::MockCryptoService;

    const BAD_STRING: &str = "I'm a bad String";

    const GOOD_STRING: &str = r#"{"id":5482,"list":[0, -5, 3],"json":{"name":"user name","age":6}}"#;
    // Be aware that the string WILL BE different when the mock get called because the timestamp should be within in
    const GOOD_DEFLATE: [u8; 62] = [0xab, 0x56, 0xca, 0x4c, 0x51, 0xb2, 0x32, 0x35, 0xb1, 0x30, 0xd2,
        0x51, 0xca, 0xc9, 0x2c, 0x2e, 0x51, 0xb2, 0x8a, 0x36, 0xd0, 0x51, 0xd0, 0x35, 0xd5, 0x51,
        0x30, 0x8e, 0xd5, 0x51, 0xca, 0x2a, 0xce, 0xcf, 0x53, 0xb2, 0xaa, 0x56, 0xca, 0x4b, 0xcc,
        0x4d, 0x55, 0xb2, 0x52, 0x2a, 0x2d, 0x4e, 0x2d, 0x52, 0x00, 0xb3, 0x75, 0x94, 0x12, 0xd3,
        0x81, 0x22, 0x66, 0xb5, 0xb5, 0x00];
    const GOOD_BASE64: &str = "q1bKTFGyMjWxMNJRysksLlGyijbQUdA11VEwjtVRyirOz1OyqlbKS8xNVbJSKi1OLVIAs3WUEtOBIma1tQA=";

    const IV_ARRAY: [u8; 16] = [0x49, 0x27, 0x6d, 0x20, 0x61, 0x6e, 0x20, 0x49, 0x56, 0x20, 0x53, 0x74, 0x72, 0x69, 0x6e, 0x67];
    const IV_BASE64: &str = "SSdtIGFuIElWIFN0cmluZw==";

    const QRCODE_BASE64: &str = "iVBORw0KGgoAAAANSUhEUgAAALIAAAC0CAYAAAAw03Z1AAAABHNCSVQICAgIfAhkiAAAIABJREFUeF7tfQmcXEWdf72zj7lyJ4Qj3KeCGkBJyGQSkFNAXcN/PdaVZU0EQeWGkKNzB1QQsohEXXV3xTVZD4Rwm0wuDiGoyE2AhJD7mGSO7n73//urej3d03O81z3dk5nM1CeTme5Xr15VvW/96nfV7yexgTIwA2WfgYTM6pjM6hMOHuVlHrcssUz/7spXj4vEKk/c2dh0SiyqH33Ask+MycpQWZMGpV1WZbpuDPVVxiSFMddlTDbjsrTfc73tCpNf1uzU/zSsX7RWKvsYBh7Qj2fAk9my5RK78koCMC9jvvjQYelk6pK96eTnbNscz1xzuKRFmCdrwKqPcRe/vVzMt2IfLRBk8SMD1/iRbJdVGsmbBoCcmeGB3yWcAU9iiTn4SYCCMlb3jV9EX9lyYKrhpK4xHfNkT4+KZ3k2iKxFNNoDiAXYPYBf8vCNIvuVOsAoIZ7QzlyAWZVAqDuoVMLxDDTV/2ZgmaewKyUOyhMnP3j4h27LfNNNfsMF1WUMwHUIuK7NGQwP7IYsA4MAflGFAC1LEcndWmQDRT114KZDewYIS5wHmDJlmbJizwc/Trv2VFcHe2ulccV2mCtJ3QNu/gQ6HlMqpBqz5fYBIOfPzcDnImYA7ACTOBsxpG7hlxo86WFPA9NrpwT1BZ9AlLOIhju/xfNcpsVkyU7v+FyzdFxpG+/8sQNXDtkZyII4Omnxr9KK8nXmAsCODT6CJLJi2YauJoxLhZIkR9nomPzJrY/f+jfQ/YFSwhnwCUOulE2tt9KL/AslfPTBaCoLYm3C/L+lVfUMZrV4zMWWL0nAVpmGC90biw6WKlPJRVsfv/1vrC6hDlDkUO+fUxWSxLGFzmZs2wYxb0sfw5vikjnfVgsouD+hsKmfy87/6LEemibxG23yr8uEggJ62WVVAWLP86TIpAUfmKo2htlJEuLKTByh6tAq1IjRtN5YO/9cv4tgvgdK3gzgBRFgtwFkAqhtlPg9N10ElA0AO5740FjHB3gvATctbKH0VWvnv2nr2snMbgGIiQqXsXjQs2lRWTLtZm/t7CrxJLGgBoAMVTybCsBQWXomdEMdl69e/z/VT364/5R4s3WYWaGdsrOh8ZhBujZEi0VG70unRzqWUc2SaY25doRJMkDo0sumf1gI+C0rJotFLaboyUG6ukfz2PamZnOPUhXZcbiuvfvh3oZNnzzp6E1HNEY+WL48a0Bo15upL2tsNHaCxGxq9yAAOwtiNn7GMyxWcz6zm3oAxBDuFE2WXJmdUBk/7p0VN7zP2DK8NzFX/RHIpKhXBMVtD9yLr3888vbm9z6+r8UY76psnJEyP2Z41inMNiUWgS6UiA7hVMIP9PbcAiVkD59AgMsgXb2vzudMBwnsVJ8XXOPf4R34bUiuw/kIxTaYkzaZHI3scV353eFV1S+B3Xw5rirPbXniO+/5DeT88hfhUlBsX2vQvk55vtEmTn/IilZMZSkD7FDr4MrzMJpQWZVkKcoqTfPixnUzn2RQ8bGcBd9fgAzwQgWUWA68tKV2Z3/9V0M/2L1vUtr2LjHMdJ3tOce4BCuuwCd4EfAggBNgaWuToGZyuFVJFEwvAJ1BqfhOIcsUoCwBsh7Yt3wOmtqgdsFg8hsUjnT6rHKXAr5QaJem71DVTOEv1Y2okb/E1Eh9LCI9um3Fd5/LdKH1N1FrzoYIVVi76939IkFsl+RGxs2/0YjHf8jsBoyzxGq1jvpIwp0+WKpKt9zctG7mD/NBTLcc2kBOJFR26mwvY2nKzNHoi5eMb7SM/2dZ5hWWZx3lKrDzK6QpIssTB60LIkCAEACTXFwk7qMcqqT8N0eLBCQesj8HOoGcBCjF95uhfjig2mQLUNX1lXr0fysH1/x+y/9eva1NSwTqpWMxmBKxHz4FHD1p8RXbZfmPnkM6YjTfuvXkj6NEn4l4yJWykmr8pfPigqs6a/UQBDLANnWDms/vDr1syeSW5tS/m3bqCy4pIHXdBy5ni23mABlEDmUCaw9Qmc7eSKffA+CeDB6Etln0U4IvgkK7hsQky2Caor0TldTfjK6u/OVbf7xmU2szy7AFT5lC6rD8faHTJ7W7kID3GvwmhkxafFqD677mSZgz10ab5Z4n8FxapcKaki+wF+ec4/eLMJvdEXO+bNfvvvkFtr1leKu+nZ/GcPQlSz+xLbXnO5ZjfcVT1Aj4LEyBSRSN6B1RKwCXXkZPUNpSz6oPbCK4pLNVsDBBuGXw2aoSebVSU//jS2Mqf7l06bSsAJsA65KQiIwWUIRAVTflgcq1uw/sd2TsTrbZE3yxw9S4olj2LmfNzJGiw0JD0VHn+z5FFvwpURy+ShPg4+5bc++1SWbcYHresR43Lvng9eDMIpXBXNrRzPbodxi7C2CTB5kE07AK7zK8cwJ1RI39d43m3LXjqdtfb+2SAHQItWKOhmLc3E0spkJXnMJ9NKllLGR+VqGhsCV2mBwdvq3+5j2MdpYcd9D8p/ddIHN3v+x2efzF9x+xuaVlps2sqR4JapzfBYA9/HFIgjf/Vbb5jMFD/SepClNiTILHmSbJL4Kfmtu46qbHs4CGDJFIdEahW7fwaN28Z9Kqfj6zmnsAxLQgVehBImyMJ52zqf62FzoS7vJH3weB3HZ7GUV8mycvNj3jc54OAFtcCAEvCUGt7Gqh/OnsbZ+54EiUV2UqDlpYYJMdZ6Ou6Heaa25f1mVvYbGjXS527rwfpWKV32VGQw+wE+iRJHuyWilFUy3fTK6f9TNom7impMu+0m1BFXrPdUxsAv31B0WCR7Pr3WNJ7gUeaRycJOlvLdJT9U5h7WDPJPHGDgBNbAdeu+NtgnZmHlsz6z99HGQFKB88Q8bPu3p/RfxnrtkMQZMMPGUuxFJEBsuVduqB5lXTr8NuyhdTmKeWv3NhehFUJ8dZ+5jJPxiz3bF+bEruJS4BmFwFOR9RLk+r/M7RxILIufidYW1Ik5BfuOrOLxl9cW6dXIlfol2G0NUDKj6XLGRQaUVHqFLzvr976xKf4Ax1xqBCKkuwG4dNWjR2u+u+jLNENL0YX89oKKIHmtamX5pf609VK3uTP735n3s5kLMTXJdYpT6/8rklpuR9y1MhodstGAsBuJz2/RyVl8AY1F5kraB/9ENaEH9K6TPf4LALcrlLXCAOJ4sB325CZyi5lcR/T/zID9XnOyhRTo/WCq6XQR0IlRa0AbJp7nDXzj6MniIWES1QMd/Dxt9VtU+1Gkl73nMaipjC0s5Otn7mKL9P2cUlvujy//I6eXT56C4vSmDwZbZcHJnR6hZPW7PquQfdiC7BTZAm1wdwqUEMapUxRMiQ/hU8T1JhbcOcEtCIczHSnAWXZH2b5xm7q4cM3lohyVsam1L7WhQlqUlSi+O6xujBNXY8qrCN2xsgcMmK69BpYC9WaduR6proIFtRRu5tsY5w0i2DdN0baabNQbIqM1eL4XnY/skxkSyKwqroG2hAFblxpljqSCtMV2Qc2Dwmppz9Hn8FGWLBNRR8JR1Q7VddyInwZqOB9oCGQlcUEP3TKqKnv8q71FaQ7xIp/sXeR5FzBlF13g9PbHLtZUzTzuAU2LaggSglCwEq5JHtmV4wAZdO8vprG8dz8FrdaCTymiyrfx2kac9bqvLaWZVD3ljx8FcbwkxuIXUSMDqs2XHMYRve3X1qZSTyMTgifcZzrU+bjjXGpXOYXE8MnJEmhuzlQt1WAKg5K4R2YtIQ1/zSvlUzfpejDfC3BlyuTaxwtPglcATqGQ2FrIAJi7LBhjlu33Ozng8r3OXPbe8Cco4jSHzC4gUp2ZnuEWUgZ23e02IpUe6wW8GL82Ngsrl1DEAhfwZJbtRV/dm4oj1drctrNq+44c38CWvzmS868lFGyfUt7vImXFy6FP+NBomfTRQQvzsXaIil+uCvr57dkDYvMkyY1G37dFcDqAmUjoHb6SycQrxMwO4KlkKpUCJG+j+M9Ynr2whSvnAXnzB3TipePctL7esxDQWDhqLGTn7rQP3Mh4oFMU13LwFyVvE+bNJdJzV47mOOqh7PqTCduC0FH0xCDhkMZJBdDl7AF34VqqRtiKja8iF69E+bV3y7Y+CS3wIVcn5PcEaWwEffZDhkfrn4QuNHoeU6B6uq1XG/vXfeOVOWxTbu23J5k5W+ypScC10ZoJbRDRsHPLnM0JHBByDWKxSWTP6drZ8L4Y4/DM/EAvKJx6jahZfs0JQVjPtQlGhY4kEd/08aCrlCZqmmn7IX508tREPRUYMHH8g5Fpuq8xbe2Gx7P/TAK3L+DMylmPCOuh72O652Isor0fYspVNMUyOvVGnRnx85uubhv/3yqv1tW4IFaeqx8sH1+c3tEQCXmNOh2ymdVn52/7Yvt9ipW2wPlJoLwQC0a2fVkNxKpssy3PpOGenEX1+eICuREKSWTYG1bLlzxOfmH761UfrIUzjrQmxWeXFBfdLjMmtO/YW9MOfT/mjpmUWvoPJ2OAhrPjWoq1ulrmPPP+no0fM88wBRhG5SYdqqQYWIkpPeFM5skuM2wA3ywaMq1B+/89iNW9t0bepDcH+cSt5uNJFFT2bQcEtznYANPV2eS+oxl//4pB1NTXcarvUvrooNxKEj+NhyJHKb09hRknTmh6vu2MBaVZnZXVAaP3ePF1WGYhH0BF+MhRWV4b233109Y7CYkxz1X5GTdLCA3Lr6Dp8094ztrrLO1bVKZjTT9o9r3aEI3LdQxYqHVxg0TUx+qTJSMafhqe+saAfepQBvDzukF/meOrktA+qsI9BJl/+86sPG3bMM17zZJUsnWI9qs+WGxvoZP8rhQVvnPzpxxqq0XlXHTBxVKgUL10lP+dekb4cmSMZbGSM5R3+wcsbmIB+KrprLvXYQgJylBPHauf+WUtSfe6T1cTIqtbBdz69HPhWYIawHyTQZzLB/gMp/1panb3mttabw0e3j4M0ft/+ZBM85cxiMGVyFRrvcX9RXl+L0ybCWlTde7tcSAPaFu4oJ8xa2xCruYOn9PSTcEY2KsWFm+vI962Y/GsaHopPRtvu6h4Gc3UJikxfeb8iR6127ibgATCS3KBReiN8iy5heAQBDppGV34/SlJs3P33LB62Nhfb2Kvzxve8OzEWiHj+TaGdqX3x2bvjEBRfvVrXHuWmf3PjL7cpK7yk6SMbuMKexfmaiOxqK9oPqSa1FjlAXmbT4KUONXMCsRkwhzXcxrARZ3cDTahEsABBix6sfYVdO27ruunf4QMkZfPZsjDDY4aSjien73xHRINVgbihX4Vs8asLC4btkb5dLU+NBuCtq/guZIbImVim60fyouW5uZncopIHAuj1Dkf0TBhQDQaqd9xqLRE9lJvHDxR5axMSQi6Iah/7X/GBoRP3m3mdu+3MrgBMAcJ/mfQPfWxEVcoS7c+ds9SL6aGYli98JQ/eAQEzmZ3MTW584RtyW7UvoZgIqlh/I/lY29vzFNX81rHdhKhvOzGKtRmQnRpf1uCSDjcC5+9tSq6ff3TrGIkybAfNzyF2O1M36o6FWXNEzvsWk+tOgoZDZsfGqmo1PfKcRW4BQ/ZV4ZovjS8N2goQKHNm+4AsPjvib4W5BZMbugJiosMS0Gkkz2MpjIvrIVhCTxxaVfstGBLwQeg8o1RPm3GDog66AhgI8RZl9KMjsryqy7GlskOWM4yDm/SgPq1c+iuxT4su//NORT2zftdlSQEBto0gVD21PFYqMX1WS/M0DK+/4GX91MN9CqIEWorfrfgOAVs7Lvt549OQFn4Kac4PHYNbuCbdMiYA8WIqZTd9LrZl9X6mFu/wpKw+QfZ744in3D392d/OHluRFmVuEsp30jmRl0uA4Zlh/H8bki/esvXO7GET5Vnf+JPXdz2KOyAL4+11vpxxFQqjXnjg46uD0c7USNYz/Ta+d+eXump/DzH/pgezzqePh0/qCYn3kaFJ1URYjimegqLIMvaNrpO9n62Z9lw8ox7EozAD7cR16t8JKOXHGX+CccxZ8V8pvuSOLKu2epvEu/J1PFPNfeuEu/72WmEcGBfD51PWSsdHRlGpmIaRSofwYD+IMM6arw70v9cVWEJOg0FVctPzR9efPCQIP5OIJM+9i0fhZXENR6HsodP7ovSkRRbZs72Ox+Cf57fyUe+fefYU+orP6JQQyX3XcqiSfO+8fCNg3QjhmF6piw4qOwCvKYnvHKM7Re9fN/oMQEkhdUR5BobPJ6bPf+5a7IRMXfNbSq25lBvyEyu0IJHy6cTJAZiNldvarT9/Swt9bVgCnhVV6DsB/SSUEslh1kc8kVuAkx8eKUrFxr6hqRTWsv9bJ1ihuiydWIsEB3MudeXoL7AEezBfCI1Tv95yncYqFLKc9YLnD+CPVrMZzrt1eP+PlDoQ7en9le4cBztghX45/2rVi0ry7W9TKS5jVUDgvBilXig6RvVT6T/aaGVfU06NzDp2G7El/r9a6a73XuO8vXhROQz1xXIn4Yr1S0Vsaf31gfeJBX7jjuzNKllcv49vpPkUmign3x8ETEv+cUmK3MIvcMCmkZAEFsQyYhmiLbvpXbM0dV/A7ibfKCX9VQGv9t6rPF0dqZz7gReMnCZ/usuuL+WFWyUi9Y65PfI1PfpaByIL4rDn1bOztD/Lrvl67lC+qm0AWMWqPnHz3cftl/Tcunejg21gBxSMQV0hDbPvexmfu+Ia4sw1vVUBj/bgq16lL7rDJcy4ztKprcdID76HMIObCHR1mddipo4aPbX13GeEukeCQrpwwfxYbUjOR1dR8a3gd0iUQq0i7bQlLN5jvrEpFPnfuXjeiDEE+tcJs9wTiSLU03Lbv373qFqFeyxzBKeEgD/mmfJXnaRfeM+SNVHKvpwLDDoUVKlTQLmSmuEwkKUqU1aRdHByd3vbgqC9wDq5d8JUGTfs1Q7YEqFMlZsrsnMPi8eeX35jiu26JrLHdoMi+cDcx8Qj8J4Zwq10hrpgwdkiRKqk63fKTLIi5ZqIwil7I3B+SdUFQfDC83tL0gqeB0DlwRCkriDGR/P1Vs6jp3tQOxL6APvTcuZ9uVJRf83RldA7QwUkHRDvYsDNVz1+F6Hc3iGn2hRYHZJ/HGVQ7/18Nrfpy5pDtvoAYE7QlxWqkqJX8bePaGdf43ekRoeCQw3JCcKTauMQDLBo9ocf4YqhI9fT+R1rWTb+Hz2nCJ0Dkrgtd/9GX3D1qryK/4FAsZYf7m+P9gtUxkq4VjZ1dVTf/Zn4fxS8pQSl8NfjbweHn/cfQbfaBPfx0R0E+rZBwEbxZMYznnbWzxokxlN/yU4K56n1N+Nt3dd28ixqVyBOcLy53jDaeWSkuq7a91V4984i2748bP7i2Qq2b34AgNIPaLywAnvYQnCMcyeRjdtTfsakUx50KXw3+NrY1vWcl38Y82sbCOsaT+RISrmVsygExDX6AnSh4mQh98ekXfL+iyfWe4DGgCxW0C30m7aQynch22YhqJxNWwH9/WYMYGzfnVVtVByEyKkCdL3CiHk4EeTggv8NOCx9yEfe4cKKa0//CgOyzFJFxc7/D9NjpCJwSXl+ckXDBJp1oR04XfaATCwPWukLxhPokS3DK92qy+TmPArY4Bp0CL+x9FvpgeL7ICKkwSHEv3PZoYg/4gsz7a5VtYBB7hMViH+fY6Kw/JEuRfjsSPzY6fs4dvBvdZDEKGLigAJ/+6v3VcMm8j3kUEITiToQp8GKTUdfV2DBJHvf2+tuahC6xi3xyYZrtr3UyfhTnzFoIP4rTe8QZSFhdWcw0F+1bOfNp8f6WkwstithQldq5S4zKqsvhtN8BJc5/WcADQhYYqrrwuAu+P0L40BBrUlwJfyM/oMjYKx82PuJSvjmK5Bf2wCLO1kl6Fat2nBt3rc5T0xTX7/57F+lfoYcdUbt4nBWL3yEiDOVv36WeHhysBF+sG6nnWtbOnM5bzwh3vrr0ArA4kJeu4xqKUAUsBjBEIdG2GMYf+C0+xkLdnlcpHJBJEoX1bsjEu+CEoteB9ylA2U7CXYUcSe9e2bj2znv584XvRDH97ef3ZK2du5m52oNnFT+BXs4iWEJVsa2kufrO8eJRnHL6cg3kG+DjaTgJ1TC9VsRZp506jNyDBWinPVOLjht+3t0XEca44FdECQFkrBw/CUmDk/4tTnvSY/wtJeCJrW59ppVes+i8nEkIuHHgcocz4FOsaO2MPyPNhMr1xZ3xoR02UOiXFFAFif5w5m6Ewc7kd3Og5ck1HB+e1LB6+tpKx76Tdl8ejCVcQRDGNNtjpkQqCL+tcLdmawUDeZnICFhVN+8mT48OZhZOGLCgyI/+A7iBBHyxokzqdBIK7XF/re/7tFTXzb8uHR0yuUfO3ZEeQalk1bb3ze0vzHyTeyJ2mllJKB2aVs9YqBn7nuex3ciZKLAASzi1AmxVVdXOuYlXT9QXTJUDVB45ZugJcz3IamSdIbYi4D7qjdAXx+3Ur5L1iW904NYXOMSBCpkZICp4pTPmgnnHfGjI73s9kg5BvL9oyng4vX7WV0MdV8qYyqck9De2y4anA8sOEb4gbQpRfoTSglUdp0p8bGV10mFw0DVF9qlxbMId3+cCHpkYw4DYV7VJZirJQUwlUT5f1DAD7bt1iJgI7c6HabaeRyp1ocgN8x6KHrTQ92uG+QEHMZWclCidNsttDKtUiviJwGWXITs8cdJdY4w3BsLoWLarR1lsInJNU6HknwWULh4iBIuLL74/Ysixm4V0HKZTeDqZI3HWbrAa/4LoFHk6hWH+C+h5f6maEC80WjvrlwiqcpjvX9zFe+vmxPADvyI9w4nxCsEXMy6AheR5EaoL9gYkP38sYjU8AUEft4ZhMSD4QR2X9pQbKbi5cOENr47rfEL8FVGfSi52NQrNipi7oagAreaoFDGM9ftW3vo035IG/IqLQ5evahteO++StFb5r/AuLEBbVNwjoe9HHOkITnq4l77+1I37itL3J8SzP3bSUVfINh2z0rEQgoQ/svjZlocT8y+tenEBb4Fi2IUsnVQU4EtgZRmO8T2uG6RYoGEKHPskqJhHD674J14ddvUwtw3UyZ+B7I64l3kresQETZQTiSVVo/GHDWtmPl68XEP+xsuUDciDHXec71JoWyg1gik66e5gaEu65q0UXk0EYiTWKrh0DOSpSzloF9cvnOnqiCnhkPEjjIAHxTkmIuq2LPngT9/byaXcfFVNcJ8GavAZECquPyebVvGcIWU3QROIkZ4h1fJXe53vmdZq9CjilVx5JddvN69L3M+M1DYebJJkpy4L8cowkiCuc/XkRbfwqj4Wu7wNFzsCssT8jPIGdIKcEvBkcUGF6xxV2TBYau3C7/Day6cEr8KgZvvjdU4AEOJq4oIbzEjsnLIf5W/1g7Hc00Z/7DNiynONHkW9BBg3xCmQSoX9C0tSkKkwxBBYQxq4ZtOYyZ8qsBiIv/ZAnvoQp8axc+Z/GStDC00JPOTEg5SKaBRzeQcoP8UANeZTUVgBgODPOxKqtiZJugen0UlODnyRhT0jtza2fPKDIX2/pJz7+vIrTQHAEjhz+bJR85rESmam32Aq8rjwVKpdFajqoLLztEjloAt+8Hlec+rLgWxteyD71DhltSS4oOoh9VVg4U5BKtwzWXL1jNm8+pXLAraRwEb7YQUCrADQzrS7nk99uVVtdNJDr2Zx05nR6gdTSuGcgqyjaNHI1Yy0t5QPO6h4Ms4CSixpGQJLS8cGgD+ftfCjWlbX3n0CUtSeyFVugfnb0CsPaWfBG8eYsoj3kdQnA+q2oNfV/npCCMbxiXMfQgxpqNoQL6+Q42PtWwz4BnyxXiErqV31yXV3+pqCEEJZQKttLicEBbbWJ15gtv0uT04URJUJc1DFwT7yiRM/d8/hAktdq+LaUuRtn+NbmC2Zt+DEK6iBC5VbUKGtCUFwkSn00hEnJHjtRN0ANQ6atnbXwRfDmWrQ5MW1aSUyteyqtowfjG212GsXTRLd6TZf3G5U/As/T6GuVNxCkYhAlYNZJWDPQ3aqLY3m90QbG7jc0FnJBTKEPJGgMOW6/8azaoZRuVEKLOiZdVn/xXLir3jCwhLwV531+JD8Pmu9O2Cbz7rCqy0ES1fsZID4gPZIANXoaEQcN+vIGajY5vPvW3omZw3Mtbc8ItlGC8WHC9RgSIiNgihJaTd9LW9OYLPTBZAFckJImIMvWHKxJ+MMkxvSHE05nA/YbJAXEUJeYk6nD8sf38BnfwZ8yEZqZz1WkIBd7ATKpGGqYIM978aPnrzl1a6dgYp9SJv7vAxVjkj6vQzRbUHwAhyKuNAH7w0lfvj59wtNSiLRKVXOAvnU5Xw6k+nGaUzD124YkyT4H+J5JO8fO+pv2MS7nkgEMuZthtjvP2A7p0hNE+d/2dBrLhUn0svpKE+6/go5ajU+u2+17x/eExFOl47lwD3hiEH3cGsfpVIOkqNcyF66znbZTdM4TE6d3eku5QOZLHnCMcV0zSuYS2bFUJFgYFb0WEzThZBHGUQHSvgZECFX3eNxfOwAcx9mLlKFQYkQvoECa2ac5E2jOV2f+Ky4u2shqsAndFGd2E1P+sfD1zaokvIyQ2AXsBddU2UZ7AXENMeyvsIbFtqUDudHAJlSwqKMvPD+8/lBRhcRxwMteeRcQuZok6XW3fob/qClUweoMZ+IUKU1sMrGLTtX8TzSPLBKGKNBqPbzKvH3Bb5YZcNl3Td69LDldeoGrnqLyMqPReeC/C+IvTA8V5H1wy64T4Tk8lng/BkQQN62lKO8Idn4dYQ1woSGOD5DZB8nakGN/yga5ZPSKenPf3C//+yfGpYnTr+BRWOfKvsBUjo3GaliVZ773R2rbn9duA/08OFfn724aOgxvyabA2m7AjHD2YsI22um/pljZtuGDikyfUk/AoDnzm5guoZ4BHCGloOcocluHpFHRCou2PX0956BgwkF0RugyKFWqHCd1FRQAAAgAElEQVSUPwI60q1N6Y88isYDLxnaekPdXmglfgK6QlbTqSfsdYlLQjnJF/qM0PW50cfT6xb+zVSkM3D+E6HWuohS5VLf4aeRcjaz52Yd3dljZDZlCqfKR13642MxwEFgSGh3ywqBHd5JUi/UN47JOIipJGCiHihhZgAvUlDCbfubVnuI61fQifQwT8itQyCGw46UNps4iKkcTI9En72IaerDwh5HC7iLIoPVIg9iZo35xOd/MYjX5LJF2yKz3adxKtBkpi5GjARUCmEEIb8KqFB0LfI4b06cfO26Q/lP7q+ffZYiXpuY50aix7UPKVXKiSFjFZBgS+wIVfb1xQdZzz/6MY6ToVr8j8yEcEvq2y4LKLgETUskxj5s3ncerzqnvbObzOpf5w03mealor0Qnm7+BhhXI0LIe/bYdiuky77124tgKaDqGl238OSUrM9ArDbMRNggN0VMGp32gB9FteTesqX+zteEvvgg75yJ2Xznfv9x5AyXwIqSTAbq2eXoEGGLIm8dSDcLjCaETJd7DwAoosXYrlHLE5zjkFKXjdJFSVbZ/gZ27KDKR3ldn4kPvK9fV8ha77bb1mrhEISAgOXii8lJPhKXo6l99QfqZ/yAT31P6IsD3zEpBARrEJH11dw4wiNhdlUoA4INV2V7sqg1rZ3rBAftaZc8MArjroCnFT4FqH+I+SZdtqpt3rB8GvIsUAnqiKjVr/9PCIegyLg59yMtAjJekUNQkCxS5Iz5fhSSaRnptfMniVba85VFtt7926YKv4moqq3iuoagGBjEJ8MTAkqIMfw8Hy9tx8OBvMM0Po2UYHQRtYMkZyixSchUtXrenu+mx/8eKB3PAMkQcAg6atKisWY0cj0C/KFeuVgK8qMgfbHMRknSubxD3MG9FxEbn0+OSZF1HKBk+OiyAJO0OCsr2FsvbBD5+xJtOQcO5DSzz+WEmHiRoII5QqNscDz2FK/aiV4vqJn+cz1rNf3INZ71KEgTxRgOJBhFzhBRN7WKVTj27NY0YaX0Ly6yW21uS8zmrMRxsYpXcPqFWFXgMMAGQUY6NYJT1kwIrXm440BOGuaZBM6QR1FUZjSzUVU19bxzDwkbOv97oLSfAZ80RD+TWOpqMQS+RibYcrEUPChOXNbS6Reb186ayzuTCABI+x73wDeiT+v/dHUTjGotMIzgmSGIKGht2nIEkPN6KXurVqmebZwGcgwiEcBHSRSfAPEMZc158/fTtvO2QJ97YOQFPIJvQ1ig+BHj8XUsBTRRsqo47gWHoJF1d30mHY9+kwL2lQ3EmXN3pslOP3noBDEE/j572fvhPWsV+HRZfZOzqkECH533A3chy94pvAXfNVSME9zJsUs2HA4hbzjM0vgUJOghCxPUJZqkvi4aoIk66Csex8YTqu8mCNCiPzziDX5EdH3P5+N7GNAkawiN0G43DaMReMFyRpSneBSSzoboymQ6ht9hsMHMW+8Nv30eV9Ojb+A9EbvV9YKDxwV5fibT5sl+96l+6zuVjT3NhzOVk3b8BAh6/GEyEvNofxGNTQtg0ss5Y+irEDQBVLiOCsdrPhnnXP2zIWzc/DFHT/nFKN4DYTonKtBzYPZfS3xi4lcIqVBJgfrKRo2JpZApKE7q/j1/vnMVj0fRabDBcr6TAtr2edwaJr0Was8gz1Zo1ZCkXLvw6mVD+JMo9oVfVCOqniCZADvjnlehgOlI6t/5/VOngsQvzbTVQ7/ReYpJRwJMgsgcYyPO+/7pCLU0xZCsSwzD+Pjzb23SEJqAbfpoI2PnzmvQFe1H5urbwTNmdJhlluBJSyBJzmETF07YoSpf51qKcvHFIjmNIpvpd411CZGrsFfyxR3DY08aPhQqbaAB3AARIRBSV9alVW+8hXN8bB+T5hDrxAVHuWl/y+Ee99kA2xBUoCaR4Itx9KDYa7yqr0YJuq1k14UaySMQUyQaecKiG9iE+R/uSrf8vVG2ZhjM+RTToWGnHUYGaHX81qXBpq7Okc+d985U7i8t/GJL1qd2DWHCfS3BDtd+0kOMBto321UrxRe0QyI5jYSYhscybTxvUsgFwe+yFM/vThs+ds46fuT7Cnx2sNCDiagkO6TgOGnEkCP5oxPZDsiDqqNHhFcxAh1w4Ni4be8HoiGhRunOeMLd6wuhPkDik74/V65b4Li6fA+AeiSjTJ9Gs8UsJK3E8Ri06bMRADN58lmNNqKJnvDzt3a8LZ7HKXN52IyE4NvgS/FLHFuK87ggIDfhxllgLZL21TgbxLyvbFw7fTc3QZcok2iBPSm8uq+Ce3fzli08rGAYRyaohz0QqY1p40T+QP+wNP0p7zO9oziP4gRoLEg/Rw8Dw33Zlybv8nte/pWfE3YrOunuqfLEBUZSdmfCbwleUS02M6HO4iOB84mI+0+Sgw9S/CZPPtpynGbbicSPiU+a/QCv7wPOH0dpfmXye0xePC6lxv4VDkHUsWBKU8zTSUuBYJG6eWB5w+oZv+H8Yq8wQRc2GM8Z1JLdsMIoDvBKkxaxFm2KrHrOKM46KgHudFzOAwVwFXP5jeP8jCflIWqihz4Y8XKGTbrrJDZxwRtphT3kKo7OAezZ6BAAGuhy6o8XpxgR5Z2lXP3a46744ZE8j0mQujF/trr8nGUp9ljWkzy/B4GtHIXzxRFZTqf3mWvmXskfUc5XUY4x8F2Tsd31324Wf9Kmhc0rqGBKa6rjw3i1pcKTjv6UNRWx9QmkQQGZcTqPT5ai4sHlLlm1XtWkxdP3evZbTJVPYSbSXvGI+cTUBwkH+X0kYQE341DCtiZTBJOWlpfu9ft7U7R2zs/cCJJolE1LAaojq+CLGRuDJDJilH38dI7pwSiCV0EY66pwgdBlKeYcJqplWVvZk6UqcTI7YAeUKLicxKqrK5AosLWUgbXgIOYDUiYseqJZjyyARgVqWH6SANcKBXBObyUowZ0krEPmlHOmLEOYUZ7Epfv8q5/1atTku842VAoNRWbXMvHFJODZ0DK1NN/2warb3j4oR5a6Alvoazk0RJLTOE9IcVtCEBaZNaSs4fwxOeiTk44XD7UtkeUFQFZcd6/f1+4DoN2giZ0QINYnznvNiSgXeUaDI5TlXLXSzYL2XVBlnAF7ff+Wf+ON+Z5YxTdMLIU48bHDTj1DZw7KxlL4rpnMaHmJvbzobt7n5SJ8a/H9P2h3ZmGo6jjAF7IQKTMt8nBrUxCqw4sKZPs8aX6N1s94LlQfCC9HrlsoiVIDmUDMByeNn/OyqUZOY3YzeGFaqt2gwu3GAwsYBmyYqW/zS350pXbVwn7hv47YRAiRWqwampPyGD6I35YiigzFTF1FpJZ3r6+o2jqfS0GBtSi3B4Qq2JA0JBLmdXPcIwiMoHQhOAQ6HAk8pQ0rbGrLUP1qrZRI8EHFJs69y4tXjiUtA9c2lLpkAuQx6ZSxX/xvn9cqkr3wWYoRExacbkiRa+noetlYCjJBI2TAUCZfWl+fSHPXzL6iauv0Hfq4w+lPQmVg4QvXBRGy/ONRWS0HLoRQRLc+gc4B+kE1poqAh4EPD1OBOphIuKd/4UHkJZRuRS5jojYBTHuYhjupgwB5LvSRf9/1vohgUxR7kWUpdnnWM64IARuCInTSpy6/5ikRpGgq+cvda6aLlAi9zTWzy/4HXJS41ShcoRk2UrkEjq+AwtmDcsBrmjgx8H5j8k6eeAdJUUrLTuTPEQXIsxmC/P87vxIQIC//bv55ynI+d5Fxs+9CXIoR5WMpoGpTo4qUTu5Kr09cxZ/dh0zQvL+lKhmi3UHgc7yMgLBFbToBn+0Mf5Kjw+t2PzPRGmX7Mh6ui/PFZSykUSCzKGOHD730XuEW2EWAvHY9IZYC+u0jLlx0vKlFsIOA2wr0FWjXSogvfFUbNt7huiJUbYfiifUw5mmaLX+/U6LxXDUd/5aoChjtEPwJqd/AaMcjRDJLWujhvDOmkTqsx7xCKexBPIbcL8kb+WhOPTUsW9CqpdjebD7D41Lww45BwnIRcwZVG+V1rnTdm3etnPleD0TNLKKT3bnF53FdhmykIacfNEjTIxlWpBW4yB4hgS2lzgSZB1EJu5wtM1/1kehaeV3M+BQlFXY8xTTf5h6eCgtx61xbbNdChRa8ov24FNFxc+50ItGjQY0xD2UQSknVhnzOWir1YtMaPxto31W1df260umA2Bb+7STcQuFg2BTOEyXHjVOOy1IyFHgkCGTAOoAszIO++1zXPQx1lZYiB1BM1z+gEyg9I41jRnAOzFM0ZWgm6UonAfKyo8AcgKU45vIFIw1NnU9Z7cvCUmROe1gmG3fc6DrxfK5ZCUm2Qs37wa6UJRoSbBlkzQ9yk6AegyvwNK2d5kyGNqNRKC4C7NxEfwHkpqZkBsjUbDAFCzNdftYeRMR5jXTVgacFwrQZpo5LbpASazJSM3j1RJCxXxhrPmhMPeWpWHAe5R8sA0tBqjakQK52vc/X//IqoWoL76IYZuS9oE7OmpSsKtrtsUxDKB8kVhnTdvMB5KBPtmy7OZydG3pkerZjV5ZrFuK6tp77fZRX1Mt2n4LnUdIVJo896Z+XjOYrtTNHIj+egjbhtm8yPXZG2aJnZrzajOb/3b929iPitMdBjg5UnhcuYDj+riqBSKKUAS/ePw41WGbivCjjjvW8yLYk7+REJSgpJEUgwqqBb7M+5R7yU6BSImrkO1nrnrSamdg1kPtBtN8D/1PSFU1lH+08MJc/rUNHItJzT7LJP8ORq5YyuDijhKAeBfafQKzBUd5IN5prE1/mdycOKXai3YSMHRIdjLDN+J6oZAg8QaG1Zz8yqVLJsWXIQ3TpQ06iA61E2IOhqSPq/+j/vTOCN5TIrgj+udjixwPb9uT33ub+1WHigRX7rPz7yJEIadhSjnN1gs66dehIJFiKv+58ZwUZUiAkgqUopdmcOoXdAMcfJAcJatTKWt5NkaDmUOKLs7PvY6dB106keBXh/VOgcIjrGX/41vbk/Y3pj8I7gHmuhIj2xwwbNCYfD937TBoTYSbWJT1kPLDuPTF7N6gADDBkiPn+yoUL+fe5Wel9lmJo3bzPp/XoJLAU6Gs5nOURXF2vYBHbXLB11U1/P/RUbXnvyz/dsWPH/jEe93MPYc+Aik2ybXZyrFKc9Mk5aidX1VRtlXgbbrDghodBymdbWpIf593KOWqS183CP/pm4oqogsCI1BVilnuqkHsnqLLn3MZji4GNgMkcIQZwxg9/0/nAvQ77PddShMlyX3C3/YTmyZa30+sSQvA8VFVteXMTq4qfLtiKEJOGkAAepuqDfc2b8mvLEcN6BwFa8H1IKkOymKScwRsqpXXPj1gU0yoeZUaYuLn5Q+nGZzJ5QgPhIoPQ86tW/5a3xEMMiKiPSt38v+IgKxxNyuHZhgUrqcjF4rLTKyoESyF2px5cyN2Yu2Jv9d/3vpYWgSUyuHVZiPUC8qD7n/CJUR/xqrNzHOur45FtPDgLlxgDjCL8YfDQt42zxTOzDXXZhzAXEZGHqm2luLlMPsAjfpbrqFCH/cFCtlPM0OJfjNQtuo6qDDtv4Yls4vx/uJqGFAFJzsN2eGt3viQKD+sdYhhf8+rTt+ziWopDTtXWboIgCgm5A/7bnwgXzpjEEgVHSzXjiSVfa+Qt+pihP+X/+cJnt+LozG6KAwGtRNdUgFKWwdnG8kyE2OItkfIvYCWJmiH+b00qqOv6oyKMUgi+KUTDBVXBgVFDcpaw8bNa9prG23Au+ZiIS1Fq4Y56JWK1RVKp9Yhh/BNuqaKzhId68S1yddcuo8A1NaHCGXsyjwKLLAlviunhuGvFq3zmtDMtRYu8Ec6iBrCDenu2rZ42BTGVqdDZ6pKVDbylCkX5DVeQK+UKvdpFh+kMLjkuRbQ4Fz8pjnEZNG1cBlB0RbYsNrnyk+fxHoU5Et9F1/vMpTlC2/XGxi2nM53y7YWITko6ZPhZgLn4Bx/n1KVtfNX5VlmpRl8CmaYGu6bIRH0pn0O0gu1qNAQ/57tglmQS/Tx9Dc/c+jjl78ObJXt1QJ9K8uS8RjAX4pArnl0mTzwZDsxkvfPczz/xxCVGr4thXI5pzbTpKwlMJk3mLGQYjQXdCyITVeT1ohmRdi/TJAdyxPPWUyXEng6mrjyGMnLoHGi6SDQiqGimwe79zqrhIoq2Gsm3qfMBtvPuPbHzu/nKDp6Pzhvo4gpYCiUGQbvxd4e49a7jOfBTdTRbKWCIaEUI3Mmk7zfYoFjsed5oXpQrDuThkdgLLM1P+bch1x33gigUvBZdW2yHvmTfcd0ivvXVcHE19itxd9AuUcQzDuYt3CGIzt6lk8bauV/iXUkczA719LOJOAg5wLHMcxgdDpEQabPLQmwFnMjx64M/XfuqmLPZbQgcB/Lrj397BxjpJKJcUKNdU0CezwEPd82jTrqc7ORUhDGjy76EveirZY44bNiy0Mm3w7bdG+rJ5MCssmGydj7vTm9Li1DuOUrM4aAdecF9H0MoChxwoCy7ARTZxU6NLLuaHPUFPWqhLcsJICMYNYomw2GH3Grppi4L6Vwh8VUPYrta2OW8alFn3jp5CFfLeNKr//31lqiiviLYi4DF1UlTve5r7hAUk2JW+sFdq6c/fwg7BHU+9T5/nHKsKTh1Toqbrgknb4nYSzjUa9qf+UcejLJtkVmdSBhZoUUfE5cCQmdRJZeiDuFEtesJx5bz3y+tyiiTfFuJ/CdWHv/X54sf5koyUrtTaxPX8vEkgohGnx91+wH44RcMx0SoL8AmDH9M2iscwtEVaQVvcPT2dsRWZsNFwshhuraCmdxRvB3a2/VGxt4I3iZtpS/m18KermjXUCdf+MLAEXrsYYmsjpTXr08XABYuXhR38YhYbDIfCmcpDm3PtvavTLCgp01ZVmk51skIn4B3G6QV4osdlk+bfWlMhaDIfiTP3PZx4mE5p6Ybn/jOewBMSIsa55Nd+F3IIz97nxD6Ak9XtB9W598I9uIfK65tQJqH1wR7IYJ6d35PL75Cak0lxiKWuXjL07f0jgykB2O6fBZ02/7NX3UR7YmzqEGaIXKugPZKk9VXl3LFQlZYbAtkogqCOsBqosGixk3DwXwL6f7g0rjfbPkWbzCxvB2579Zc+exFVI7+XBgk+qr2gqx3MZkZ6U04zn8Hn5N+4hDU7v0vHcsjCrWY1jV8MwrlgEWsrswqo9H/4+35uMhvm2st2LNL+e8aVfk1T+AXxqJGej04cJiOIVRIwo+3dNysr704tlr+Zd/VXtDig0MQBPNRVfokPteH4nF+PrCgwn1IvNOm/GyIxewzfLYimGUkVhdOZPFI9Df8CZ1kSRBA9i1qu5+69UmJx3vgjhcBFBYk3jEdD1nbh194j4jRm2c2DBpal9d97cXf/njDflVW+qr2gnTGTLNS03c8ecemQ97HuKsX6rMV7+3dc61HjvQIJtlVdX6N69xJJSzv/uhP397IvyOvxA6KADLXyVGMXajhFH0F0vOSQNnhDW3a4FuDh6ztyen8+9IbR/iKjan6T4X2olyWtjajKtEH8jGOU3DxN8318xfxRvsrS0Evz9dWpO30TdwIEsr0D/YVrC5CqfwXn7+pL3eqiPCBjEqJKZwtqFCiP0EqX/Ihyl7jrXRQ6PAmjglZTDrj2PPvO4rX6OzwZge3B37lay+Oj1b9F/e9kGin6AsqK9JSICE05vG4eHWdGCeX2AN2ucAZ6ZsV/ChOwy66tw5Gt0Ghgz0SW4GMWEP1CAgZSidsBV3KAbI4qdvw55sekyxytNdDsBeohog9PAq82TSLP2x5CaPA+26iGx6dloT24nmmcE+pYEGUd+RgFiAYwbi1lPG998jHOCcPysHs1UF7ts8OtKTSczjXyoNIBxQPMfkRfRRG4y10lpPX7oStoEu5VLfVHzimab8QBwJDsBcUscdJMVNyrx5LFheuUy6hydrnu6v12AOcniFMTMAUHOTLxFJUgKVIvmq8OPc+3pk+mKSmZJPo79BjLlhyTJq5tYQV8Lycje2ykNId0JIVTSQv6oKtoHbasg+jx/KtL6bV3M1S9EDWKU+S7QR0ynR4Ewz82+/um8m/X1ZCsPmC6J6nb/i1ZMFgw5d0b2UvyLkFPsaI2zI6rgv9eikXdXbS+85fc+bwvu4w99/t6SR7IeZekO6Y3i/YSBnGsE9WDVvCG/DZzM4G3hbIItUt2/v09W9hC99IOdzwzGChjxh3qOJaHEscnOSxe0slmGUFUV3RH+PGEZd0hL2y8OB4SlPjddvqE3v6ybGlLl4EdmbEvR475aEak8lfErlVeKyHrgu9X7CRSLj+FLGVAktdn5xpL9D5JDwqx+cKTQGR+IAiwrTiOFVUqpw4U/DKifrg7SOg2dbLU5/l/axUtftE8nfsOb2ukOEDLEU6+Yr1yl1+Lr/eunP00OQlxHPe2LPvQUr0KByEQhA4+Ach0Ser0ezZvIVEUAgijtR2hb7jLIZcOwcsA/wcYI4mxqZdzTZf0Laq4pQrDhvXz0QAAq6awz1dr6Su22y92ton6dxEs6erFeVL/xWyRx2MXYb7xGmOMuQf6+5o4NqbwKA3xTyrj9xD4RQgnB19yQOjNicPwMsHWicedC0AyC6OPelRGTk+PmTr544JO9qOwOkxPyhJVNYWI9cxnh3CI458SnGk3kGgk8pJc+/lHUiE7UZgvVZBNK6oD4APxQ0hzOiBzZaogn8SukZh13AQ0/z1ZxDTtM4Wju87mvb9wuOaipAxpLkPkQI1cESY8ztw2ezorXVEkQkknJJOmbJM/93ONw2X4M7BHLCaOJNOfIbKjq+KDH/n0ZuRk69EVNmncMchz8j7+3bvFGnAOhpST38ntBS67bxkrr7TD5PQ033oZc9LeKDGkl1Td8+ZB2TzJeZQIrCOaGZev7m/ti4zy25haxOZYJmtu3FXo+ykdbADcCRavvxKM8JAATUS+sIIWAA6BTpByNUPG9O/4w8WMTy76kO4a77J+r0/XLML6ameI28yLK6DLPRxdoprKU4dHL9AjLeEqsdwM9P7avlKgwNW4++4golyNIYpMoglLHlxxb2NVxfUmLO5QaUTIOO2KYJP/vSxo2+GMzgWFLdbh2gUOkIr7ZmqXju0dt5kHkTD964L6kzgdZ/p19X4dAb/VExS6QTKwId3WAFaCpUpzU3fJp+QAZYCc8QDzEAw/8zcbyH87lE8on+YLLBEjeWIIplpI1k/VwjLS6eFJlSdA5koIDy1KNB01JXuQ2Yhoq7hVhb8b13PYPsY+xN//aVSxyWERaj5mRtXQ9jbCRUNth1MwEEpvuHDSP7N2rD4x7wLFDOuXxdSt0nu6f/yXxXJiPwgqWRBbDphXzuaKI3FmPQ9fqVAL8HOgUyNXTmFgyS5dsYNkgmztRrSbJ1Rx+l6RXTCrP/kHUt0qCHhlwooEPrEeS1sP3dyv5MQe0QB7YesKlgKiQwfFYM/y28qpY9JyF70umo+G/nm5s1PuIg5HTr8LueNIzKocUNy7ayf8HFdWVhq4q6BTAwO2AJSpVVK7DahLQhhJ+c9IRYD4ae0+FUjJy38NA8FVQoWw7f0JdfM/zkG3sKFgx6NEccHh+0SCnvbuWFbPQRa2k77u5aC/EmAk8F1C75mReMT4OwDEhPCFM2nE1TbgW+Ky67iHwvgjXl9lDBkP6vDnTB3v6fJNaF1uL4UKjme4a2e6ac1K4EWgwYKl9HI+JnfMqJVDyJTajg+LDPqbv32WYq08QZbP9uPgcfn8aDsDd0aSqlu9jVKx198//D3kk27uGznmQTkEPiCwK7GVLhEvM6em/uxYrsUQJF5syR2cqFqkKZ/mShRqJOvdANnMQxY/PRIdOKsp/3Wuv/CfSHAWD/vJ5KZauo5qpxlKYbJ1eeL8XDhpvtj4o31xQJq6u9G77U0rBdJgsJG9CeVBgJ9YFqrpIp/4qMnQ0oRJQyQ0azv4rny9ic0M72OYjOQvTHc87C9mC1uOlLz2eoJ867nWgxfsg13f4e1WjUhg+TIV5mMxdUT+gsJ8yDjxIdj3L5n7Q3bMekDLIW/hLXxiftxWuiEwpIEcbM+0+z0L5rW3/Y2x0UXrpodIsH/MgTp92v628fHL/3x4NeaG/ZxXyLPDrl9cJ8D0OcIG2JYZ+99btZLXFVVIilfq52/2tK1WiwYG08pakV3NUn8GgVvVCuxBba8xtbPERH7BWvWf6kx8cVwUa2ZNPfKA0r8tzwtBbJzBM4lVfDZTtl2bXfNLN/Lsni2MyRF5jB0iQLREX3Jsm8myoQSUvVFhhIEXwb492vqi6dNgdWGQNxtSV/oLMecUX2BYmFhaToOxJKbYKkK6c3J6IJhqlWqZFlbx542+lOi9X7OUpDgTvm46xLHN7sKgRjTVID2kdhO7KQ1qvxFPp8i8U9IPLV/v+FWT/a+rFBzbuJ1FtFPRZR3sBhhpVMSlHCOzTDfZ+tmH5cFREjLT/v+iwmAM/+wurvO3OPZLzEKrWanbdhuuLlc3IJftGhyl62E65kEhfCohuCMxcalFKpLiSQhhSPCDdxGiYdTHbbcWnOHOGTLTfV8l+mnRVBO7sKwe+M+V3bhxEVpKcJG9Adx0CvUiNGywlgz93OlmMRCgYxnikEcc96PRm62kztcifykw7IYdDt0hnqFjKhGa9ma2bViEN0EBkVAB+9dfU7i7GZNXYUUYnF8pocJEz8nrMAoN0zSd9n1yJ/P6QC+I+cWroDADw75ysTCKZEVQ+Kx2dtXfNePn1v89sef1edLdvzyhLmbXF0dUxAx4ywFnAwcz3ZXd5+lyExnEUDGrZwpl9yaCQv+/UAs+lOWPlCY+isD5ubmh9kL877qdyYPXQW+cR/MdFfl+T/6p6qI+qndlvlxKWkMjyL6fIuZjru2UQOfkQgWk8IQ2RVrEnlDsKFAPajoGs4FSDvUyqodNYq8LlpR+fy4L9esXi7CgaQ3RIcAAA9+SURBVPmd6eaCK3BIva96ziI+d846FouNZ2ZTATsyRiTJiLoUlYaZUt2etbeu5ixFmzkubtTFAZme5QMHlrsn09HKC5lRoC6Xwkgh6SVrbP4P9sKs6/3udw/MpfK0azOXtGiRKqBIabq419Ib78oB8fg7VrDIoEvAFxdGwEjTpdcokZaWJcZzs7+TIYilGG3xQM4BjTJhfqOjS1VwFiqAT0L3sTolvVqSWg78yF2fuKE0YAZfQHHoEhvw+zHBxyYSpCoMwdP6oKXQp3R+kSdZ78+8cAZiWRBLE+b+ASeBPs/sAgkXgRiHcqW0+Za3ftYpouXS7XDdADK64m8Lw2vvPmGPbL/jIcJFQfwyjQZgliNQhxupHzbV33lzqQeYeRX+76DxhgB7XouH+secky5K7czfObEhX2SphsIosc8XKzg1d/KIwVWvL/820iOUVtYIr37r6IURbwNd4u41t75b47lfh54VwCQ1bgFUDHpH12hkTVrsJuUz838mHkP300BLXnxpj1Pnjn5K/sA+3SARqowPybkLn3Hkyi8yYx9pg8K/G2Ih4bJNB/KHMnkcBzEZkkpzBK51eoMoVLj34PPLlZMW3N+ix6/3jAbkMAuKe5vXNAkBkWokgNjziL12wef51RIJAuEGMVCrzQxMQSaD5cuF9bZ27l9YNH4WSzcWRompQRkqTaVKHmynr2uov/OBUvLFuf0tDZC5vsrnQc+d/zSL6Z8tWJrlgybn6jiOulgve6tnnM0PsPrWowGY9eAM+FqpEy/7wbBNTcYrpqociYAzhYMYbulSfLAcazqwNPlcYlpGQVCOkZQKyIRjtCVYCq1u3lvIQnlSYXZ3f3hcNReXFdPeN0rWzthaf9tH/irOsALlmIeBNsUMtBKkw+rmn7nDk1/wcDA+9CmPNrOIIN5atRK3jWeT9TOFz3YuwSvxjIfndQIfnOVr76ybcars2Htw1g8WhrDORf4DiP/C6nc0ecg2z9oyrHbh5X5a23LxzYEj6xcVck5kVNYunLZLUl/yFDgJ20lExCyAJ+aTRWq2aoUlU//IgpgIXRjNUXGzXUIgUwfIxLtMSZCxpKnpeJZGYudiwYyzXnRSeo+mPKJNmHWPGB75e8DZaKCUdgaIffONEkrtwt+26JGfOB7CkyESWnj3g0yXhJpNSZmb2XOzTxffcg1FWTVCJWQtcubW57GOPisxanNM3eppWNEip3NhzpY8/7ACgbeCMcP668iYdtFOim7JS2nVN6VFRl9pLTuHo85LnLrL0R6HeX8MDirwCQ7nGJ87VoBYiSm67W2fPnncUQnu3dgz76nEFNkfFB1rApg3vZTYcSxjR0s2SKsaK4LNIKcIzKfViK1K/uQew9o5aOK8q8RTBqhzN5aL1CbU7bgZd+60tdeRf5RATCZnNB3mdEceiOEQprvujmlnVB3DQdxNj7ZCxlceipzpga9xOHL8XaO3SNZGZMSOQXDAKi3GZ5hWOwJmyJVwxDafHV6lfXkbDwCD4odnKmTg/bYuuV/yU+1I3Vw774TdrrQcmRjPYB5CPriI6xzagy13BoknrlA0w9ry+ZEnHU/xUHpadVpeINNYfX1kzcdvH9w0rOZdV5aGFqXN4PMGVoNkBi0uKXD2UT35ZmPNHT9sndL+Hm8tF1v5f+ct9sikRXebrnuLp4L62klse8T1cYGssEJapmilLKXTb3lrfNPzQdD/F97xwoYpavunQaZMSeiP7FJfM3X9BL6FFWo0aX22T50lOPc77sZBsn71/pU3r/EfBq+22SD63fBxLmaMvfWevMU9ZNL8f2pwZUTHVIYzhyK20sGBYnZIDJhkGL1KYqa5hq2ZMVFMAQRHnuGrZ0vPAJmPKWeA5y5axSqidcxoBIWlUwWF8mPUHjkXQxKmeAiINae63p+Praz45tuPXv+BmEKYQfszoPMo8LALfjC20bZ/asrKJ5mLGCW2SWwEySBFYIDmHkJ4pJop6X2/dtbM/5qY854R7MSz2v5fxCA6aibsd9mBKpPnL3Xk2DeZ3QRMIpRowbpK/5m0tUloF5oNyXIQN0xbfkRF7LYsoFFPBNUL6QEXdiy9sp7v+ZcNzn74Jfd+Ym+LcY8huZMQoQQATtHJmML8JdoMFbuhDOcJOcaihjEjuW7mAnG5dJ5sxcxsDwOZupgdcM24edc0acqPXQXqSscsfovjIwdpJ9KsVDAK2R9VI/83TPUSHz51++utE0NBzHkI/0OM7SD2YdoGJZMCjMZ7+MX3f2ZvOrnA8NzJHs4QAMDERhRPMPgkko44qsgIz1ph2lc0rZ8pQqL1gqNfBwHIfEboudieGKseN/esFlVd7USg0TDIUbvY7Y43izYx2XSSWkUUMS4QKn+p1LU5Dc/c9Dh/Mi9YTJQKtk+DGuClJIx+/rrMyCpqF3/FUryZlsxOBtklQY4udZNIkD4frWjVkgzNxCjmjt+2ZsaWcjkAZcZSyO+DBWTRR99wQocYH93z/jNpTceR/gPE/naTcviAJiEGgOYxwV13b1ySlx5RMeSBdx67ZmubSSJKTSmDeZT98plRC3kx7eti8SXmKIyc/vPAe/RlS07e2dh4vcG8b7gKnVfEMDiAicUoUpBr7QARBqg94aKrW+Zyc7V/ALeXOXMdXCDTZOWoaqomLbqj2fMWcl7OIht/2NPZ7V976zcklVP4WSUiUTpY2TSZKusvRGX1f0bFK3/7zqPThC669QYIpVOPlQWwCdTlNa123vNc4L4P3qutJuD4i396xE6r+Z8N1/i65Vof9xTKLAAhzsE2RPSzWJkjC2Ba1DgoilMdtstqJPaN/fXTf8Uv90I158EHspgZssXzs8xHXbj4tC1p53GkskJsXcRK6I56qPWl0CvhQiEEFaCZMkPBeiUjwmhU01/RFG1ZRbTykW2PTnsr9xbxNwmoOLM3lY4/4ejU7NlohwOcLnP2qBsFjaAJAt4cPINTWzqe1XG+5dEX3vuptO1+vsU1/5/lmie6FOqXjoAj+yyP50EyQlEaoPwR+FRYryIq/MpIU7psy/rbtolaB1eoy+9p5nMvAbLfnZztKjZx8Q/TzLlRKOwRLJoLFMWo6fKHDgrrcV4DsSuyoEY2JmBTakQKtGer47GnBkcr1777h2++mX933mccSgXQt+F8YCFFgDVQizLm8gc/mbasiQdSzRcarjkZeNcZcrRwlpdyklNictLFy4V6p3XSWS4M0nIAEwYqHPOcm1vWzBQGp17GSuSPoLAXkH93OT7nsBpDxs89bZ+kLkNwiVOZBKpD22boYDBhOpcDasp7TJno8QtAB2uTAgGXXE3WXqvWK/9iSsarg1jkueFHD9m2Yem/bA/Tetg6h3/xp0eoSePIPVbqHF3VP9GcbjrTcZ1TePApDlyi2gAuZb/gCRcJvN0RivN7xnXycAFApCZE/4nY9rpRjnPl5rV3+uM8ePrh/J529rn3AVn0lPSh+PHVZOPvuk5W7SUuZX+wuWdWCYSYDqaE2I9MtiiZgI2dmstK5ERD4EYEdhc/tJ60yN7B0ejGpCHtNVS5cURF/KOY4uyALjvZ1JhOWTZ04yiqripV1ZGYJCmVKeYdtvNAajSgWRWJyCMOpJLHo80aHh2J2ARaSJxbwQ9RXCGkgmVAtBmeZqIUO1LuuGkhw78CemFK0KhYVlNMsf+9eVViGa/Vy6lwm5Hkfuh1f+dYp+rqfhF9Qdm9xLCtf/cocL5ZCrVS0IjJt0OG8EQUC4XCaEEC5QAnHtn/yK8J3AJ8PgeUidbFM7sRRSWiR/f4tIPX97kLnmcIi8j1U66RClKCL1rJgct76hdf746UhTLysSDL6F3plbfdnnO9VW7Jvau3/t1bKXLb+crx2KLk3DvM5AOm7FzsEWvYqiclDQehqNyFtBgEQKJm2DEyIM88lnxv6HJu6eg7AiuPR0d9pgo91Xdfz06WUNPC2Wbl4ZMqq6+j4JS8yzlznTeKXv2xB158qcZPrMYc/CT4ln00TK8ftRj3ObJbyy1X4GkBBpC2cmzBpRrDwWoHOwryTyG2ncYNRZbBdE9/9MiK+A0bn/jOe7xXQqfvbx8Hq5/FP7cPATkzyLaCx5jL7v3Uzsb0IoPZF3gqecORKRZ7NTi/kknzxc/vwb1T+KEg1hp4MagcZQKwpP72qHjN9HdWXPu+6BzFmJiNP/u22b4PAtnHRp5314mX3nvshy3GTMszv+Fw/SqEJTtN2z8MK/2ISmf05RIyL3KrJuxA0PZEPXXJ4bK64B1K3tNKE0AUDhF3174L5MzLyAP0xRffH1lpGNcYjnEj3uKRCP4tNA0uXOPA0R6aoPYFRcqd7FswEYwd1Fd7PcaUe/avuuk/W8FLUX5mgwIfIgDOjKvvAzlLXmS2bLmUG6K0+qIfnWUb6etSjvVVT4YwSNoGSmLoIOMQ8Ywl18e2dqbMf0DQRAIA4qHAQAlLJQgwTj2T7NgYUZRf1ej6QzueujHr+ceFOFKf9G0WorOJPYSA3LpfwrNtKTzbpoG3yJbBF3z/MsN0rgKoL8WGCgsZUWoyMJC+Fn8IMQd8dSkNDZ1Ne6Hfk7BG2hIAVyFVDS1K0jljYcJlFVeao6r6pxot8rMdT920qk3rwnWVFi2N8JAthyCQW98VaTgUduqpXn4g6cMuvH/C9mTzlcxMXQEgH8kqBwmVLU8XwoGd1elyql0q83gQjgA2CQYKh5TNvk65NQUE6a5pRwERpiA+qv4OrIB/rHTN5dufue3lduAlv5B+FNP5UAZyzrsl1R2UtacubwdqSnLY4joX7Wxpvsi1nVqo8Y5gVdVQ7ZJVD01wYx8BHMphfrKCtMREHEHXSRGoZHKRhNEF+zpoyllCYCWDIedVYYym6PmEX5meS3YXUovjOY4JbDssokXe16ToysqI+uQwSVnZqvfNjJJM+2+8QYvXt7IELZpD63o/AXKblyYo9bbD4GnWlv2gWudMWRZ7b/+mT+Es4Dlw2Dmr2bI+JTnW4Y5rxMjHiJvJuY8OAZyAiP84yPGbA59ayd/Fc6aZAJrhXvjXaIt8dWwTxBacg6I1S5r2YUzSXqmIaS96pvVSbc2Yl3HEPt/MglS38KMWlLdfgpdmL1P6I5Dbjh/xvVg90DQcLEhHYEHtq2/42ZDfvvj+6NFDq4/cvD95fMzxRkhxdcx+wz48LstDZEWucCWpOmk7MZBs8hElPoDQjkIsggf6K6crZCmFZFKNjuM1p5i7d4iuf2Q0Gx+ymLrryHjknS2NxrarPnH8R0uWfK0xt5Otf9clqF2G/mIVJbhhaKCIGfj/kaKUBhx2zr8AAAAASUVORK5CYII=";
    const QRCODE_EMBEDDED: &str = concatcp!("data:image/png;base64,",  QRCODE_BASE64);

    #[derive(Default)]
    struct TestData {
        mockCompressionService: MockCompressionService,
        mockCryptoService: MockCryptoService
    }

    impl TestData {
        fn default() -> Self {
            let mut mockCompressionService = MockCompressionService::new();
            let mut mockCryptoService = MockCryptoService::new();

            mockCompressionService.expect_deflate()
                .times(1)
                .returning(|_: &str| Some(Vec::from(GOOD_DEFLATE)));
            // Test for the addition of the timestamp
            mockCompressionService.expect_deflate()
                .with(eq(GOOD_STRING))
                .times(0)
                .returning(|_: &str| None);
            mockCompressionService.expect_to_base64()
                .with(eq(GOOD_DEFLATE.as_slice()))
                .times(1)
                .returning(|_: &[u8]| GOOD_BASE64.to_string());
            mockCompressionService.expect_to_base64()
                .with(eq(IV_ARRAY.as_slice()))
                .times(1)
                .returning(|_: &[u8]| IV_BASE64.to_string());

            mockCryptoService.expect_encrypt()
                .with(eq(GOOD_DEFLATE.as_slice()))
                .times(1)
                .returning(|_: &[u8]| Some((Vec::from(GOOD_DEFLATE), Vec::from(IV_ARRAY))));


            Self {
                mockCompressionService,
                mockCryptoService,
            }
        }
    }

    lazy_static! {
        static ref test_data: TestData = TestData::default();
    }

    #[test_log::test(actix_web::test)]
    async fn create_errors() {
        //let test_data = TestData::default();

        // Initialize the Actix testing framework
        let test_config = clap::App::new("TESTING")
            .get_matches();

        let app = test::init_service(
            App::new()
                .service(super::create)
                .app_data((test_config, crate::Context {
                    compression_service: &test_data.mockCompressionService,
                    crypto_service: &test_data.mockCryptoService
                }))
        ).await;

        // Test random bytes (not a valid String)
        let req = test::TestRequest::post()
            .uri("/create")
            .set_payload(Vec::from(GOOD_DEFLATE))
            .to_request();
        let result = test::call_service(&app, req).await;
        assert_eq!(result.status(), StatusCode::BAD_REQUEST, "An invalid String should produce a BAD_REQUEST");

        // Test invalid JSON
        let req = test::TestRequest::post()
            .uri("/create")
            .insert_header((header::CONTENT_TYPE, "application/json"))
            .set_payload(BAD_STRING)
            .to_request();
        let result = test::call_service(&app, req).await;
        assert_eq!(result.status(), StatusCode::BAD_REQUEST, "An invalid JSON should produce a BAD_REQUEST");
    }

    #[test_log::test(actix_web::test)]
    async fn create_valid_default() {
        // Initialize the Actix testing framework
        let test_config = clap::App::new("TESTING")
            .arg(Arg::new("endpoint").long("endpoint")
                .default_value("http://localhost.test/"))
            .arg(Arg::new("signed").short('s').long("signed")
                .action(ArgAction::SetTrue))
            .get_matches();

        let app = test::init_service(
            App::new()
                .service(super::create)
                .app_data((test_config, crate::Context {
                    compression_service: &test_data.mockCompressionService,
                    crypto_service: &test_data.mockCryptoService
                }))
        ).await;

        let req = test::TestRequest::post()
            .uri("/create")
            .insert_header((header::CONTENT_TYPE, "application/json"))
            .set_payload(GOOD_STRING)
            .to_request();
        let result = test::call_service(&app, req).await;
        assert_eq!(result.status(), StatusCode::OK, "The request should return a HTTP success code");

        let body = test::read_body(result).await;
        assert_eq!(body, QRCODE_EMBEDDED, "The response's body should contain our QRCODE as embedded base64 png (data:image/png;base64)");
    }
}