# Errors status in Russian
error-bad-request = Неверный запрос
error-forbidden = Запрещенный
error-not-found = Страница не найдена
error-not-allowed = Не допускается
error-not-acceptable = Запрос не принимается
error-internal-server-error = Сервер обнаружил внутреннюю ошибку и не смог продолжить

cannot-parse-bytes = Сервер не смог прочитать ваш запрос как UTF-8
cannot-parse-json = Сервер не смог прочитать ваш запрос в формате JSON

status = { $name } работающая версия { $version }