# Errors status in Chinese
error-bad-request = 错误的请求
error-forbidden = 禁止的
error-not-found = 网页未找到
error-not-allowed = 不允许
error-not-acceptable = 请求不可接受
error-internal-server-error = 服务器遇到内部错误，无法继续

cannot-parse-bytes = 服务器无法将您的请求读取为 UTF-8
cannot-parse-json = 服务器无法将您的请求读取为 JSON

status = { $name } 启动运行版 { $version }