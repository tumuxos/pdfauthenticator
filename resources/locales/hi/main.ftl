# Errors status in Hindi
error-bad-request = खराब अनुरोध
error-forbidden = वर्जित
error-not-found = पृष्ठ नहीं मिला
error-not-allowed = अनुमति नहीं
error-not-acceptable = अनुरोध स्वीकार्य नहीं है
error-internal-server-error = सर्वर को एक आंतरिक त्रुटि का सामना करना पड़ा और वह जारी नहीं रख सका

cannot-parse-bytes = सर्वर आपके अनुरोध को UTF-8 के रूप में पढ़ने में सक्षम नहीं था
cannot-parse-json = सर्वर JSON के रूप में आपके अनुरोध को पढ़ने में सक्षम नहीं था

status = { $name } अप एंड रनिंग वर्जन { $version }