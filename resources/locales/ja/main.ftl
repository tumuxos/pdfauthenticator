# Errors status in Japanese
error-bad-request = 要求の形式が正しくありません
error-forbidden = 禁断
error-not-found = ページが見つかりません
error-not-allowed = 禁止されている
error-not-acceptable = リクエストは受け付けられません
error-internal-server-error = サーバーで内部エラーが発生し、続行できませんでした

cannot-parse-bytes = サーバーはリクエストを UTF-8 として読み取ることができませんでした
cannot-parse-json = サーバーはリクエストを JSON として読み取ることができませんでした

status = { $name } の稼働中のバージョン { $version }