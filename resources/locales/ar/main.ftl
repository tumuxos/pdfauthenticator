# Errors status in Arabic
error-bad-request = اقتراح غير جيد
error-forbidden = ممنوع
error-not-found = الصفحة غير موجودة
error-not-allowed = غير مسموح
error-not-acceptable = الطلب غير مقبول
error-internal-server-error = واجه الخادم خطأً داخليًا ولم يتمكن من المتابعة

cannot-parse-bytes = لم يتمكن الخادم من قراءة طلبك كـ UTF-8
cannot-parse-json = تعذر على الخادم قراءة طلبك كـ JSON

status = { $version } نسخة قيد التشغيل { $name }