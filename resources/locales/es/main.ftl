# Errors status in Spanish
error-bad-request = Solicitud incorrecta
error-forbidden = Prohibida
error-not-found = Page not found
error-not-allowed = Página no encontrada
error-not-acceptable = Solicitud no aceptable
error-internal-server-error = El servidor encontró un error interno y no pudo continuar

cannot-parse-bytes = El servidor no pudo leer su solicitud como UTF-8
cannot-parse-json = El servidor no pudo leer su solicitud como JSON

status = { $name } en funcionamiento versión { $version }