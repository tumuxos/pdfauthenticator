# Errors status in English
error-bad-request = Bad request
error-forbidden = Forbidden
error-not-found = Page not found
error-not-allowed = Not allowed
error-not-acceptable = Request not acceptable
error-internal-server-error = The server encounter an internal error and wasn't able to continue

cannot-parse-bytes = The server wasn't able to read your request as UTF-8
cannot-parse-json = The server wasn't able to read your request as JSON

status = { $name } up and running version { $version }