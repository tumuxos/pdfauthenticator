# Error status in French
error-bad-request = Mauvaise requête
error-forbidden = Interdit
error-not-found = Page non trouvée
error-not-allowed = Non autorisé
error-not-acceptable = Requête non acceptable
error-internal-server-error = Le serveur à rencontré une erreur et n'a pas su continuer

cannot-parse-bytes = Le serveur n'a pas pu interpreter votre requête comme de l'UTF-8
cannot-parse-json = Le serveur n'a pas pu interpreter votre requête comme du JSON

status = { $name } tourne en utilisant la version { $version }